package com.nostratech.example.app.vo;

import lombok.Data;

/**
 * Created by yukibuwana on 1/30/17.
 */

@Data
public class AdmindukResponseVO extends BaseVO {

    private String nik_ayah;
    private String jenis_klmin;
    private String no_rw;
    private String no_rt;
    private String telp;
    private String stat_hbkel;
    private String agama;
    private String tmpt_lhr;
    private String jenis_pkrjn;
    private String nik;
    private String nama_lgkp_ibu;
    private String stat_kwn;
    private String no_kk;
    private String pddk_akh;
    private String nama_lgkp_ayah;
    private String nama_kep;
    private String dusun;
    private String tgl_lhr;
    private String nik_ibu;
    private String no_prop;
    private String kode_pos;
    private String no_kel;
    private String nama_lgkp;
    private String alamat;
    private String no_kab;
    private String gol_drh;
    private String no_kec;
    private String _index;
    private String _type;
    private String _id;
    private String _score;
}
