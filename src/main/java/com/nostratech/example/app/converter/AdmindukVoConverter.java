package com.nostratech.example.app.converter;

import com.nostratech.example.app.persistence.domain.Adminduk;
import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.util.ExtendedSpringBeanUtil;
import com.nostratech.example.app.vo.*;
import org.springframework.stereotype.Component;

/**
 * Created by yukibuwana on 1/24/17.
 */

@Component
public class AdmindukVoConverter extends BaseVoConverter<AdmindukRequestVO, AdmindukResponseVO, Adminduk> implements
        IBaseVoConverter<AdmindukRequestVO, AdmindukResponseVO, Adminduk> {

    @Override
    public AdmindukResponseVO transferModelToVO(Adminduk model, AdmindukResponseVO vo) {
        if (null == vo) vo = new AdmindukResponseVO();
        super.transferModelToVO(model, vo);

        ExtendedSpringBeanUtil.copySpecificProperties(model, vo, new String[]{"nik_ayah","jenis_klmin", "no_rw", "no_rt", "telp", "stat_hbkel",
                "agama", "tmpt_lhr", "jenis_pkrjn", "nik", "nama_lgkp_ibu",
                "stat_kwn", "no_kk", "pddk_akh", "nama_lgkp_ayah", "nama_kep",
                "dusun", "tgl_lhr", "nik_ibu", "no_prop", "kode_pos",
                "no_kel", "nama_lgkp", "alamat", "no_kab", "gol_drh",
                "no_kec", "_index", "_type", "_id", "_score",});

        return vo;
    }

    @Override
    public Adminduk transferVOToModel(AdmindukRequestVO vo, Adminduk model) {
        if (null == model) model = new Adminduk();
        super.transferVOToModel(vo, model);

        ExtendedSpringBeanUtil.copySpecificProperties(vo, model, new String[]{"nik_ayah","jenis_klmin", "no_rw", "no_rt", "telp", "stat_hbkel",
                                                                                "agama", "tmpt_lhr", "jenis_pkrjn", "nik", "nama_lgkp_ibu",
                "stat_kwn", "no_kk", "pddk_akh", "nama_lgkp_ayah", "nama_kep",
                "dusun", "tgl_lhr", "nik_ibu", "no_prop", "kode_pos",
                "no_kel", "nama_lgkp", "alamat", "no_kab", "gol_drh",
                "no_kec", "_index", "_type", "_id", "_score",});

        return model;
    }
}
