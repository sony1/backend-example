package com.nostratech.example.app.controller;

import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.service.BaseService;
import com.nostratech.example.app.service.PeopleService;
import com.nostratech.example.app.vo.PeopleRequestVO;
import com.nostratech.example.app.vo.PeopleResponseVO;
import com.nostratech.example.app.vo.ResultVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by aditrioka on 3/2/2017.
 */

@RestController
@RequestMapping(value = "/api/v1/people")
public class PeopleController extends BaseController<People, PeopleResponseVO, PeopleRequestVO>  {

    @Autowired
    PeopleService peopleService;

    @Override
    protected BaseService<People, PeopleResponseVO, PeopleRequestVO> getDomainService() {
        return peopleService;
    }

    @RequestMapping(value = "/get-people",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getPeopleByAddress() {

        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return peopleService.getAllPeoleByAddress();
            }
        };
        return handler.getResult();
    }
}
