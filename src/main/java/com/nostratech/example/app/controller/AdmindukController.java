package com.nostratech.example.app.controller;

import com.nostratech.example.app.persistence.domain.Adminduk;
import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.service.AdmindukService;
import com.nostratech.example.app.service.BaseService;
import com.nostratech.example.app.service.PeopleService;
import com.nostratech.example.app.vo.*;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by aditrioka on 3/2/2017.
 */

@RestController
@RequestMapping(value = "/api/v1/adminduk")
public class AdmindukController extends BaseController<Adminduk, AdmindukResponseVO, AdmindukRequestVO>  {

    @Autowired
    AdmindukService admindukService;

    @Override
    protected BaseService<Adminduk, AdmindukResponseVO, AdmindukRequestVO> getDomainService() {
        return admindukService;
    }

    @RequestMapping(value = "/post-adminduk",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> postAdmindukByNik(@RequestBody String input) {

        AbstractRequestHandler handler = new AbstractRequestHandler() {
            @Override
            public Object processRequest() {
                return admindukService.insertAdminduk(input);
            }
        };
        return handler.getResult();
    }
}
