package com.nostratech.example.app.controller;

import com.nostratech.example.app.persistence.domain.Base;
import com.nostratech.example.app.persistence.domain.People;
import com.nostratech.example.app.service.BaseService;
import com.nostratech.example.app.service.PeopleService;
import com.nostratech.example.app.service.UploadFileService;
import com.nostratech.example.app.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import javax.swing.text.html.HTML;

/**
 * Created by aditrioka on 3/2/2017.
 */
@RestController
@RequestMapping("/api/v1/mkia")
public class UploadFileController
{
    @Autowired
    UploadFileService fileService;

    @RequestMapping(value = "/parsing",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getUserAttribute(@RequestBody SingleFileVO fileVO)
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return fileService.parseFile(fileVO);
            }
        };
        return handler.getResult();
    }

    @RequestMapping(value = "/uploadhtmltodoc",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getHtmlTodoc(@RequestBody HTMLToDocVO htmlToDocVO)
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return fileService.uploadHtmlToDoc(htmlToDocVO);
            }
        };
        return handler.getResult();
    }@RequestMapping(value = "/uploadhtmltoxls",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<ResultVO> getHtmlToXls(@RequestBody HTMLToDocVO htmlToDocVO)
    {
        AbstractRequestHandler handler = new AbstractRequestHandler()
        {
            @Override
            public Object processRequest()
            {
                return fileService.uploadHtmlToXls(htmlToDocVO);
            }
        };
        return handler.getResult();
    }
}
