##Backend Example

Provide API to add, edit, delete and read

### Requirement :

1. Java JDK 8
2. MySQL
3. Install Lombok Plugin

### Prerequisite - MySQL :

1. Create database `testing` in MySQL
2. Username `yukibuwana`
3. Password `welcome1`

### How to run :

1. `cd backend-example/`
2. Run using maven `mvn spring-boot:run`

### How to test :
1. Import postman file in `api/Backend Example.postman_collection`
2. Now you can test base on that API.
